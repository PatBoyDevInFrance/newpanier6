<?php

namespace App\Entity;

class RecherchePrice
{
    private $prixMin;
    private $prixMax;
    private $nom;

    public function getPrixMin()
    {
        return $this->prixMin;
    }
    public function setPrixMin(int $prix)
    {
        $this->prixMin = $prix;
        return $this;
    }

    public function getPrixMax()
    {
        return $this->prixMax;
    }
    public function setPrixMax(int $prix)
    {
        $this->prixMax = $prix;
        return $this;
    }
    
    public function getNom()
    {
        return $this->nom;
    }
    public function setNom(int $name)
    {
        $this->nom = $name;
        return $this;
    }
}
