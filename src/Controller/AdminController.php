<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\RecherchePrice;
use App\Form\AdminFormType;
use App\Form\RecherchePriceType;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(PaginatorInterface $paginatorInterface, Request $request, ProductRepository $repo): Response
    {
        $recherche = new RecherchePrice();

        //form pour la forme
        $form = $this->createForm(RecherchePriceType::class, $recherche);
        $form->handleRequest($request);

        $cartes = $paginatorInterface->paginate(
            $repo->findAllWithPaginator($recherche), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            6 /*limit per page*/
        );
        return $this->render('carte/carte.html.twig', [
            'immos' => $cartes,
            'form' =>  $form->createView(), 
            'admin' => true 
            
        ]);
    }

    /**
     * @Route("/admin/{id}", name="modifAdmin")
     */
    public function modif(Product $product, Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(AdminFormType::class, $product);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em->persist($product);
            $em->flush();

        };
        return $this->render('admin/modif.html.twig', [
            'immos' => $product,
            'form' =>  $form->createView()
            
            
        ]);
    }

    
}
