<?php

namespace App\Controller;

use App\Entity\RecherchePrice;
use App\Form\RecherchePriceType;
use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CarteController extends AbstractController
{
    /**
     * @Route("/carte", name="carte")
     */
    public function index(ProductRepository $repo, PaginatorInterface $paginatorInterface, Request $request): Response {   
        $recherche = new RecherchePrice();
        $form = $this->createForm(RecherchePriceType::class, $recherche);
        $form->handleRequest($request);

        $cartes = $paginatorInterface->paginate(
            $repo->findAllWithPaginator($recherche), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            6 /*limit per page*/
        );
        return $this->render('carte/carte.html.twig', ['immos' => $cartes,'form' =>  $form->createView(),'admin' => false  ]);
    }

    
}
